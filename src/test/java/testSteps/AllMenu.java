package testSteps;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import runner.TestRunner;

import java.util.List;

public class AllMenu extends TestRunner {
    @Test
    public void menuTesting() throws InterruptedException {
        driver.get("https://www.amazon.com/");
        Thread.sleep(3000);

        Select dropDown = new Select(driver.findElement(By.xpath("/html//select[@id='searchDropdownBox']")));

        List<WebElement> allOptions = dropDown.getOptions();

        System.out.println("List of all options:");
        for(int x = 0;x <allOptions.size();x++){
            System.out.println(allOptions.get(x).getText());
        }

        Thread.sleep(3000);
        dropDown.selectByVisibleText("Baby");
        Thread.sleep(3000);


        WebElement searchBox = driver.findElement(By.id("twotabsearchtextbox"));
        searchBox.click();
        searchBox.clear();
        searchBox.sendKeys("teddy bear");
        Thread.sleep(3000);

        WebElement searchButton = driver.findElement(By.id("nav-search-submit-button"));
        searchButton.click();

        List<WebElement> allProducts = driver.findElements(By.xpath("//div[@data-component-type='s-search-result']"));


        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,5500)");
        Thread.sleep(3000);

        System.out.println("Total number of Products: " + allProducts.size());

        WebElement thirdPageButton = driver.findElement(By.cssSelector(".a-pagination > li:nth-of-type(4) > a"));
        thirdPageButton.click();
        Thread.sleep(3000);

        List<WebElement> thirdPageProducts = driver.findElements(By.xpath("//a[@class='a-link-normal a-text-normal']"));
        WebElement fifthItem = thirdPageProducts.get(4);
        fifthItem.click();
        Thread.sleep(3000);

        WebElement addToCartButton = driver.findElement(By.id("add-to-cart-button"));
        addToCartButton.click();
        Thread.sleep(3000);


    }}
