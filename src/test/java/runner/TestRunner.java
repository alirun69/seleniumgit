package runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestRunner {
   public static WebDriver driver;
    @BeforeClass
    public static void getDriver(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public static void quitDriver(){
        driver.quit();
    }
}
